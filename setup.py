# encoding: utf-8
from setuptools import setup, find_packages

setup(
    name="xaxtsuxo",
    version='0.0.0',
    author='Moriyoshi Koizumi',
    author_email='mozo@mozo.jp',
    description="ぁっぉ",
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Other Audience',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: Japanese',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
        ],
    license='MIT License',
    keywords='ぁっぉ',
    url='http://bitbucket.org/moriyoshi/---',
    packages=find_packages()
    )
