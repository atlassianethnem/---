# encoding: utf-8
from .words import markov_chains, katsuo_words
from .renderers import build_string_list 

def ぃぅ():
    return build_string_list(markov_chains, katsuo_words) 
